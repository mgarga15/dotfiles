filetype indent plugin on
syntax on

set background=dark

set ts=4 sw=4 sts=4 et
set smartindent

set mouse=a

"Spell checking in LaTeX
autocmd BufRead *.tex set spell
"autocmd BufRead *.tex syntax off spell toplevel

"Smartindent for Python
autocmd BufRead *.py set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
"Delete whitespace at the end of lines
autocmd BufWritePre *.py normal m`:%s/\s\+$//e``

function! s:toggle_syntax()
    if exists("g:syntax_on")
        syntax off
    else
        syntax enable
    endif
endfunction

nnoremap <silent> <F5> :call <SID>toggle_syntax()<CR>

"Comment python
vnoremap <silent> # :s#^#\##<cr>:noh<cr>
vnoremap <silent> -# :s#^\###<cr>:noh<cr>

" IMPORTANT: grep will sometimes skip displaying the file name if you
" search in a singe file. This will confuse Latex-Suite. Set your grep
" program to always generate a file-name.
"set grepprg=grep\ -nH\ $*

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
"let g:tex_flavor='latex'

" TIP: if you write your \label's as \label{fig:something}, then if you
" type in \ref{fig: and press <C-n> you will automatically cycle through
" all the figure labels. Very useful!
"set iskeyword+=:

"Show line numbers
set relativenumber

"Open ls in quickfix with Ctrl+L
nmap <c-l> :cexpr system('ls -F --color=never') \| copen <Enter>

"Insert matching brackets
inoremap { {}<Esc>i
inoremap ( ()<Esc>i
inoremap [ []<Esc>i
