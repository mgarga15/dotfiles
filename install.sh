#!/bin/bash
ln -sfv ~/.dotfiles/.bash_profile ~
ln -sfv ~/.dotfiles/.bashrc ~
ln -sfv ~/.dotfiles/.config/i3/config ~/.config/i3/
ln -sfv ~/.dotfiles/.config/i3/workspaces.py ~/.config/i3/
ln -sfv ~/.dotfiles/.git ~
ln -sfv ~/.dotfiles/.i3status.conf ~
ln -sfv ~/.dotfiles/.install.sh.swp ~
ln -sfv ~/.dotfiles/.vimrc ~
ln -sfv ~/.dotfiles/.xinitrc ~
ln -sfv ~/.dotfiles/.xprofile ~
