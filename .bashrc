#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export EDITOR=vim

#start_custom

# mkdir, cd into it
mkcd () {
    mkdir -p "$*"
    cd "$*"
}

# Colors for ls
export LS_COLORS='rs=0:di=01;02;42:ln=01;36:mh=00:pi=40;33:so=0:do=0:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=0;31:*.jpeg=0;31:*.gif=0;31:*.bmp=0;31:*.pbm=0;31:*.pgm=0;31:*.ppm=0;31:*.tga=0:*.xbm=0:*.xpm=0:*.tif=0;31:*.tiff=0;31:*.png=0;31:*.svg=0;31:*.svgz=0;31:*.mng=0:*.pcx=0:*.mov=0;31:*.mpg=0;31:*.mpeg=0;31:*.m2v=0:*.mkv=0:*.webm=0;31:*.ogm=0;31:*.mp4=0:*.m4v=0:*.mp4v=0:*.vob=0:*.qt=0:*.nuv=0:*.wmv=0:*.asf=0:*.rm=0:*.rmvb=0:*.flc=0:*.avi=0:*.fli=0:*.flv=0:*.gl=0:*.dl=0:*.xcf=0;31:*.xwd=0:*.yuv=0:*.cgm=0:*.emf=0:*.axv=0:*.anx=0:*.ogv=0:*.ogx=0:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:*.tex=01;32:*.py=01;32:*.c=01;32:*.cpp=01;32:*.js=01;32:*.sh=01;32:*.txt=01;33:*makefile=01;31:*Makefile=01;31:*.pdf=0;35:*.ps=0;35:'

PS1='\[\e]0;\a\]${debian_chroot:+($debian_chroot)}\[\033[0;36m\]\w$\[\033[0m\] '

#aliases
alias ls='ls -Nh --color'
alias l='ls'
alias ..='cd ../'
alias ...='cd ../../'

alias m='make'
function pdf {
    evince "$@" & disown
}

alias rm='rm -I'
alias brightness='xrandr --output LVDS-1 --brightness'
alias off='sudo shutdown now'

alias gl='git pull'
alias gs='git status'
alias ga='git add'
alias gc='git commit -m'
alias gp='git push'

alias pci='sudo pacman -Sy'
alias pcs='pacman -Ss'
alias pcu='sudo pacman -Syu'

# For workspace icons
alias python=python3

# For android studio
export JAVA_HOME=/usr/lib/jvm/java-8-jdk/

# added by Anaconda3 4.2.0 installer
export PATH="/home/smash/anaconda3/bin:$PATH"

export LD_PRELOAD='/usr/$LIB/libstdc++.so.6 /usr/$LIB/libgcc_s.so.1 /usr/$LIB/libxcb.so.1 /usr/$LIB/libgpg-error.so'

# Enable touchpad tap to click
#xinput set-prop 13 295 1
